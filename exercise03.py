import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
# Import csv file
col_names = ['timestamp','lat_rcv','lon_rcv','signal_strength']
df = pd.read_csv("ex1.csv", names = col_names)

#### Degree to Decimal ######
# decimal = degree + (min/60) + (sec/3600) 
lat_dec =  50 + (42/60) + (25.96/3600) 
lon_dec =  7 + (5/60) + (49.13 / 3600)

# Calculate Distance between Transmitter and receiver
df['lat_trs'] = lat_dec
df['lon_trs'] = lon_dec
#distance = []

def haversine_dist(lat1, lon1, lat2, lon2):
    dlat = np.radians(np.subtract(lat1, lat2))
    dlon = np.radians(np.subtract(lon1, lon2))
    radius = 6371e3
    a = np.sin(dlat/2) * np.sin(dlat/2) + np.cos(np.radians(lat1)) \
    * np.cos(np.radians(lat2)) * np.sin(dlon/2) * np.sin(dlon/2)
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))
    d = radius * c
    return d

df['distance'] = haversine_dist(df['lat_rcv'], df['lon_rcv'], df['lat_trs'], df['lon_trs'])


# Path loss Calculation

wave_length = 1.35
h_t = 50
h_r = 2
dc = 930
# FSP
pl_fsp_rr = [] 
for d in df['distance'].values:
    path_loss_fsp = 20* math.log((4*math.pi*d)/wave_length,10)
    pl_fsp_rr.append(path_loss_fsp)


# TRG
pl_trg_rr = []
for d in df['distance'].values:
    path_loss =  20* math.log((d*d)/(h_t*h_r),10)
    pl_trg_rr.append(path_loss)

plt.plot(df['distance'], pl_fsp_rr, marker = '+', color= 'g')
plt.plot(df['distance'], pl_trg_rr, marker = 's', color= 'b')
plt.xlabel("Distance(m)")
plt.ylabel("Path Loss(db)")
plt.legend(("Path Loss FSP","Path Loss TRG"),loc='lower right') 
plt.show()

plt.plot(df['distance'], df['signal_strength'],'go-')
plt.xlabel("Distance(m)")
plt.ylabel("Relative Signal Strength")
plt.show()
