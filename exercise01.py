import matplotlib.pyplot as plt
import math
import numpy as np

ht = 50
hr = 2
wl = 1.35

path_lossFSP=[]
path_lossTRG=[]


for d in range(1,10000,1):
    FSPloss=20* math.log((4*math.pi*d)/wl,10)   
    path_lossFSP.append(FSPloss)
    TRGLoss=20* math.log((d*d)/(ht*hr),10)   
    path_lossTRG.append(TRGLoss)    
    
    if(np.abs((FSPloss-TRGLoss))<=0.01):
        print(d)
        print(FSPloss-TRGLoss)
        print(FSPloss)
        print(TRGLoss)
        

plt1,=plt.plot(path_lossTRG,'ro',label="TRG")    
plt2,= plt.plot(path_lossFSP,'go', label="FSP")
plt.legend()
plt.xlabel("Distance (m)")
plt.ylabel("Path Losses (dB)")
plt.title("FSP & TRG Plot ")
plt.show()
