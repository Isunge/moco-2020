import numpy as np
import math
import matplotlib.pyplot as plt
import pandas as pd

wave_length = 1.35
h_t = 50
h_r = 2
path_lossFSP = []
path_lossTRG = []
for x in range(1,10000,1):
    FSPloss=20* math.log((4*math.pi*x)/wave_length,10)
    path_lossFSP.append(FSPloss)
    TRGLoss=20* math.log((x*x)/(h_t*h_r),10)
    path_lossTRG.append(TRGLoss)
    
    if(np.abs((FSPloss-TRGLoss))<=0.01):
        print(x)
        print(FSPloss-TRGLoss)
        print("FSPloss",FSPloss)
        print("TRGLoss",TRGLoss)

dc = 930

pathLoss_fsp = []
pathLoss_trg = []
distances_fsp =[]
distances_trg = []

for d in range(1,10000,10):
    if(dc>= d):
        path_loss_fsp = 20* math.log((4*math.pi*d)/wave_length,10)
        pathLoss_fsp.append(path_loss_fsp)
        distances_fsp.append(d)
    else:
        path_loss_trg = 20* math.log((d*d)/(h_t*h_r),10)
        pathLoss_trg.append(path_loss_trg)
        distances_trg.append(d)  

###################  T L D     ######################################
L0 = 19.377
n0 = 2.0
n1 = 3.0
n2 = 3.0
d0 = 1
d1 = 200
d2 = 500
pathLoss_tld = []
distances_tld = []

for d in range(0,10000,10):

    if(d<d0):
        pathLoss = 0
        pathLoss_tld.append(pathLoss)
        distances_tld.append(d) 
    if((d0 <= d) & (d<d1)): 
        pathLoss = L0 + 10*n0*math.log(d/d0,10)
        pathLoss_tld.append(pathLoss)
        distances_tld.append(d) 
    if((d1 <= d) & (d<d2)): 
        pathLoss = L0 + 10*n0*math.log(d1/d0,10) + 10* n1*math.log(d/d1,10)
        pathLoss_tld.append(pathLoss)
        distances_tld.append(d) 
    if(d2 < d): 
        pathLoss = L0 + 10*n0*math.log(d1/d0,10) + 10* n1*math.log(d2/d1,10) + 10*n2*math.log(d/d2,10)
        pathLoss_tld.append(pathLoss)
        distances_tld.append(d)   
        
        
colors = ['#17becf','#1a55FF','#8c564b']
plt.plot(distances_trg, pathLoss_trg, marker = 'x', color= colors[1])
plt.plot(distances_tld, pathLoss_tld, marker = "o", color= colors[2],lw = 0.1)
plt.plot(distances_fsp, pathLoss_fsp, marker = '+', color= colors[0])
plt.xlabel("Distance(m)")
plt.ylabel("Path Loss(db)")
plt.legend(('pathLoss_trg','pathLoss_tld','pathLoss_fsp'),loc = 'lower right')
plt.annotate(s = "Crossover Distance = 930m", xy = (930,-6),xytext = (1030,10),
             arrowprops = dict(facecolor='black', arrowstyle='simple'))
plt.annotate(s = "FSP path loss", xy=(930,77.98), xytext =(200,40),
             arrowprops = dict(facecolor='cyan',arrowstyle = "->"))
plt.annotate(s = "FSP path loss", xy=(100,18), xytext =(200,40),
             arrowprops = dict(facecolor='cyan',arrowstyle = "->"))
plt.annotate(s = "TRG path loss", xy=(930,77.98), xytext =(7000,60),
             arrowprops = dict(facecolor='magenta',arrowstyle='simple'))
plt.annotate(s = "TRG path loss", xy=(10069,120), xytext =(7000,60),
             arrowprops = dict(facecolor='magenta',arrowstyle='simple'))
plt.annotate(s = "Path Loss at Dc", xy=(930,77.98), xytext =(0,110),
             arrowprops = dict(facecolor='black',arrowstyle='simple'))
plt.title("Path Loss using FSP, TRG and TLD model")
plt.show()
